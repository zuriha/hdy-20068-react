import React,{ useState } from 'react'

export default function FromProductAll(props) {
    // var u_id = localStorage.getItem('Id');
    const [user_id, setId] = useState('')
    const [title, setTitle] = useState('')
    const [detail, setDetail] = useState('')
    const [stock, setStock] = useState()
    const [price, setPrice] = useState()
   
  
    const save = async (e) => {
      e.preventDefault()
      let user = {
       user_id: user_id ,
       title: title,
       detail: detail,
       stock: stock,
       price: price
      }
    props.save(user)
    }
  
    const CreateProductAll = async (e) => {
        e.preventDefault()
        let user = {
         user_id: user_id ,
         title: title,
         detail: detail,
         stock: stock,
         price: price
        }
      props.CreateProductAll(user)
      }
    

    return (
        <div className="container" >
        {/* <center><img src="assets/images/Re.png" width="250" height="100" className="d-inline-block align-top" alt=""></img> </center>  */}
             <div className="card border-success">
             <div className="card-header"><h1> Create Product All </h1> </div>
             <div className="card-body">
 
 <form  onSubmit={props.check === "FromProductAll" ? CreateProductAll: save} >
    <div className="form-group">
       <label>ID:</label>
       <input type="text" value={user_id} onChange={(e) => setId(e.target.value)}  className="form-control" id="username" />
     </div>
     <div className="form-group">
       <label>Title:</label>
       <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} className="form-control" id="password" />
     </div>
     <div className="form-group">
       <label>Detail:</label>
       <input type="text" value={detail} onChange={(e) =>  setDetail(e.target.value)} className="form-control" id="name" />
     </div>
     <div className="form-group">
       <label>Stock:</label>
       <input type="number" value={stock} onChange={(e) => setStock(e.target.value)}  className="form-control" id="age" />
     </div>
     <div className="form-group">
       <label>Price:</label>
       <input type="number" value={price} onChange={(e) => setPrice(e.target.value)} className="form-control" id="salary" />
     </div>
     <button type="submit" className="btn btn-success float-right"> Save </button>
     {/* <button
       type="submit"
       classNameName="btn  float-right"
       style={{ backgroundColor: "#00ff00", color: "black" }}
     >
       บันทึก
     </button> */}
   </form>
   
   


      
  </div>
</div>



    </div>


      
    )
  }
  