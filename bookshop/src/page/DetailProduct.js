
import React, { useState, useEffect } from 'react'
import {getAllProduct} from '../api/api'

export default function DetailProduct(props) {
    const [product, setProduct] = useState([])

    useEffect(() => {
        const fetchApi = async () => {
          let result = await getAllProduct(props.match.params.id)
          let data = result.data.filter((items) => {
              return items._id === props.match.params.id;
            });
          console.log(data[0])
          setProduct(data[0]);
        }
        fetchApi();
      }, []);
    
    return (
        <div>
            <h1>Detail Product</h1>
            <h3 style={{textAlign: "center"}}>Title: {product.title}</h3>
                    <p style={{marginLeft: 10 }}>Detail: {product.detail}</p>
                    <p style={{marginLeft: 10 }}>Stock: {product.stock}</p>
                    <p style={{marginLeft: 10 }}>Price: {product.price}</p>
        </div>
    )
}
