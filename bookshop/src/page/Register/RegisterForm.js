import React,{ useState ,useEffect } from 'react'

// import { createRegister } from '../../api/api'

export default function RegisterForm(props) {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [name, setName] = useState('')
    const [age, setAge] = useState()
    const [salary, setSalary] = useState()

    const save = async (e) => {
        e.preventDefault()
        let user = {
            username: username,
            password: password,
            name: name,
            age: age,
            salary: salary,
        }
      props.save(user)
      };
    
      useEffect(() => {
        if (props.check === "Edit") {
          setUsername(props.user.username)
          setPassword(props.user.password)
          setName(props.user.name)
          setAge(props.user.age)
          setSalary(props.user.salary)
  
        }
      }, [])

      const edit = async (e) => {
        e.preventDefault()
        let user = {
          name: name ,
          age: age,
          salary: salary,
        }
      props.edit(user)
      }
   
   
    return (
        <div className="container" >
           {/* <center><img src="assets/images/Re.png" width="250" height="100" className="d-inline-block align-top" alt=""></img> </center>  */}
                <div className="card border-success">
                <div className="card-header"> </div>
                <div className="card-body">

                {props.check === "Edit" ? 
                <form  onSubmit={edit}  >
      
                <div className="form-group">
                   <label>Username:</label>
                   <input type="text" value={username} onChange={(e) => setUsername(e.target.value)}  className="form-control" id="username" disabled />
                 </div>
                 <div className="form-group">
                   <label>Password:</label>
                   <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} className="form-control" id="password" disabled/>
                 </div>
                 <div className="form-group">
                   <label>Name:</label>
                   <input type="text" value={name} onChange={(e) =>  setName(e.target.value)} className="form-control" id="name" />
                 </div>
                 <div className="form-group">
                   <label>Age:</label>
                   <input type="number" value={age} onChange={(e) => setAge(e.target.value)}  className="form-control" id="age" />
                 </div>
                 <div className="form-group">
                   <label>Salary:</label>
                   <input type="number" value={salary} onChange={(e) => setSalary(e.target.value)} className="form-control" id="salary" />
                 </div>
                 <button type="submit" className="btn btn-success float-right"> Save </button>
                 {/* <button
                   type="submit"
                   classNameName="btn  float-right"
                   style={{ backgroundColor: "#00ff00", color: "black" }}
                 >
                   บันทึก
                 </button> */}
               </form>
               
                 : 
                 
                 <form  onSubmit={ save }  >
      
                 <div className="form-group">
                    <label>Username:</label>
                    <input type="text" value={username} onChange={(e) => setUsername(e.target.value)}  className="form-control" id="username" />
                  </div>
                  <div className="form-group">
                    <label>Password:</label>
                    <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} className="form-control" id="password"/>
                  </div>
                  <div className="form-group">
                    <label>Name:</label>
                    <input type="text" value={name} onChange={(e) =>  setName(e.target.value)} className="form-control" id="name" />
                  </div>
                  <div className="form-group">
                    <label>Age:</label>
                    <input type="number" value={age} onChange={(e) => setAge(e.target.value)}  className="form-control" id="age" />
                  </div>
                  <div className="form-group">
                    <label>Salary:</label>
                    <input type="number" value={salary} onChange={(e) => setSalary(e.target.value)} className="form-control" id="salary" />
                  </div>
                  <button type="submit" className="btn btn-success float-right"> Save </button>
                  {/* <button
                    type="submit"
                    classNameName="btn  float-right"
                    style={{ backgroundColor: "#00ff00", color: "black" }}
                  >
                    บันทึก
                  </button> */}
                </form>
                }
     
      
      
  </div>
</div>



    </div>
    )
}
