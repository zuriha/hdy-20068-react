
import React from "react";
import { register } from "../../api/api";
import RegisterForm from '../../page/Register/RegisterForm';

export default function Register(props) {
  const save = async (user) => {
    let result = await register(user)
    props.history.push('/login')
    // console.log(save);
    
  }
  return (
    <div>
    <RegisterForm save={save} />
    </div>
  );
}