
import React,{ useState } from 'react'

export default function LoginForm(props) {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')



    const save = async (e) => {
        e.preventDefault()
        let user = {
            username: username,
            password: password,
        }
      props.save(user)
      };

      const login = async (e) => {
        e.preventDefault()
        let user = {
            username: username,
            password: password,

        }
      props.login(user)
      }
    


    return (

<div className="container" >
                <div className="card border-success">
                <div className="card-header"><h1> LOGIN  </h1> </div>
                <div className="card-body">
      
<form  onSubmit={props.check === "Login" ? login : save} >

       <div className="form-group">
          <label>Username:</label>
          <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} className="form-control" id="username" />
        </div>
        <div className="form-group">
          <label>Password:</label>
          <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} className="form-control" id="password" />
        </div>

        <button type="submit" className="btn btn-success float-right"> Login </button>

      </form>
      
      
  </div>
</div>



    </div>
    )
}
