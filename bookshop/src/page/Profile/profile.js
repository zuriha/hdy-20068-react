import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import { getUserById } from "../../api/api";

export default function Profile(props) {
  const [userProfile, setUserProfile] = useState([]);

  var id = localStorage.getItem('Id');
  useEffect(() => {
    fetchUser();
  }, []);

  const fetchUser = async () => {
    await getUserById(id).then((res) => {
      if (res.status === "success") {
        setUserProfile(res.data);
      }
    });
  };

  return (
    <div className="container" >
      <div class="card border-danger ">
        <div class="card-body">
          <h1>Profile  User  </h1>
          <hr></hr>


          <div class="row row-cols-2">
            <div class="col">
              <img src="assets/images/Re.png" width="200" height="200" className="d-inline-block align-top" alt="" style={{margin:'50px 0 0 20px'}}></img>


            </div>


            <div class="col">

              <label for="exampleFormControlInput1">UserName : </label>
              <p>{userProfile.username}</p>

              <br></br>


              <label for="exampleFormControlInput1">Password :</label>
              <p>{userProfile.password}</p>


              <br></br>


              <label for="exampleFormControlInput1">Name :  </label>
              <p>{userProfile.name}</p>


              <br></br>


              <label for="exampleFormControlInput1">Age:   </label>
              <p>{userProfile.age}</p>

              <br></br>

              <label for="exampleFormControlInput1"> Selary :  </label>
              <p>{userProfile.salary}</p>


              <br></br>
              {/* <button type="submit" className="btn btn-warning float-right"  > Edit Profile</button> */}
              <Link to={`/edituser/${userProfile._id}`} className="btn btn-warning float-right">Edit Profile</Link>
              
            </div>

          </div>


        </div>
      </div>




    </div>
  )
}
