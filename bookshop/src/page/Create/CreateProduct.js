import React from 'react'
// import ProductForm from '../../component/ProductForm'
import FormProduct from '../../components/FormProduct'
import { createProduct } from '../../api/api'

export default function CreateProduct(props) {

    const save = async (save) => {
        let result = await createProduct(save)
        props.history.push('/showproduct')
      }

      return (
        <div>
          {/* <Back url="/product" history={props.history}/> */}

          <FormProduct save={save}/>
        </div>
      )
}
