import React, { useEffect, useState } from "react";
import RegisterForm  from '../Register/RegisterForm'
import { getUserById, editUser } from "../../api/api";


export default function Edituser(props) {
    const [user, setUser] = useState();

    useEffect(() => {
        const fetchApi = async () => {
          let result = await getUserById(props.match.params.id);
          console.log('result',result);
          setUser(result.data);
          console.log('result.data',result.data);
        };
        fetchApi();
      }, []);
    
      const edit = async (user) => {
        let edit = await editUser(props.match.params.id, user);
        if (edit.status === "success") {
          props.history.push("/Showprofile");
        }
      };
    
    return (
        <div className="container" >
     <h1>Edit User </h1>
     {/* <RegisterForm /> */}
     {user !== undefined && <RegisterForm  check="Edit" user={user} edit={edit} />}
        </div>
    )
}
