import React, { useEffect, useState } from "react";
import FormProduct from "../../components/FormProduct";
import { getAllProduct, editproduct } from "../../api/api";

export default function Edit(props) {
    const [product, setProduct] = useState();

    useEffect(() => {
      const fetchApi = async () => {
        let result = await getAllProduct(props.match.params.id)
        let data = result.data.filter((items) => {
            return items._id === props.match.params.id;
          });
        console.log(data[0])
        setProduct(data[0]);
      }
      fetchApi();
    }, []);
  

  const edit = async (product) => {
    let edit = await editproduct(props.match.params.id, product)
    if (edit.status === "success") {
      props.history.push('/showproduct')
    } else{
        alert(edit.message)
    }
 
  }

  return (
    <div>

   {/* <FormProduct    product={product} edit={edit} />  */}
      {product !== undefined && <FormProduct check="Edit" product={product} edit={edit} />}

    </div>
  );
}