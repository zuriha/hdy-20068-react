import React, { useState, useEffect } from 'react'

import ProductTable from "../../components/ProductTable"
import ButtonCreate from "../../components/ButtonCreate"

import Mystock from "../../components/Mystock"
import { getAllProduct, deleteproduct  } from '../../api/api'


export default function ShowProduct(props) {
    const [product, setProduct] = useState([])

    const fetchProduct = async () => {
      let result = await getAllProduct()
      console.log(result)
      setProduct(result)
    }
  
    useEffect(() => {
    
      fetchProduct()
  }, [])


    const nextCreateProduct = () => {
        props.history.push('/createProduct')
      }

    
      const removeUser = async (id) => {
        let check = window.confirm("คุณต้องการลบหรือไม่ ?")
         if(check === true) {
           let result = await deleteproduct(id)
           console.log(result)
           if (result.status === "success") {
            fetchProduct()
           }
         }
       }
     

    return (
      <div className="container" >
          <ButtonCreate nextCreateProduct={nextCreateProduct}/>
          <br></br>
          <h1>My Stock</h1>
          <Mystock  product={product} delete={removeUser}/>
          

          <hr></hr>
          <h1>Product All</h1>
          <ProductTable  product={product}    />

     
    
        </div>
    )
}
