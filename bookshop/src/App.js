import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Header from "./components/Header";
import Register from "./page/Register/Register";
import PrivateRoute from "./helper/PrivateRoute";
import Login from "./page/Login/Login";
import Home from "./page/Home/Home";
import CreateProduct from './page/Create/CreateProduct';
import FromProductAll from './page/Alllproduct/FromProductAll';
import ProductTable from "./components/ProductTable";
import ShowProduct from "./page/Home/ShowProduct";
import Showprofile from "./page/Home/Showprofile";
import DetailProduct from './page/DetailProduct'

import Edit from './page/Edit/Edit'
import Edituser from './page/Edit/Edituser'

import { NavLink } from 'react-router-dom'
import { Route, Switch, Redirect} from "react-router-dom"
import "bootstrap/dist/css/bootstrap.min.css";



var routes = {
  register: "/register",
  login: "/login",
  home: "/home",
  createProduct:'/createProduct',
  showproduct:'/showproduct',
  fromproductall:'/fromproductall',
  showprofile:'/showprofile',
  edit:'/edit/:id',
  edituser:'/edituser/:id',
  detailproduct:'/detailproduct/:id'


};

function App() {
  return (
    <div>
    <Header />

     <Switch>

          <Route exact path={routes.register} component={Register}></Route>
          <Route exact path={routes.login} component={Login}></Route>
      </Switch>

      <Switch>
  
  <PrivateRoute exact path={routes.home} component={Home}></PrivateRoute>
  <PrivateRoute exact path={routes.createProduct} component={CreateProduct}></PrivateRoute>
  <PrivateRoute exact path={routes.fromproductall} component={FromProductAll}></PrivateRoute>
  <PrivateRoute exact path={routes.showproduct} component={ShowProduct}></PrivateRoute>
  <PrivateRoute exact path={routes.showprofile} component={Showprofile}></PrivateRoute>
  <PrivateRoute exact path={routes.edit} component={Edit}></PrivateRoute>
  <PrivateRoute exact path={routes.edituser} component={Edituser}></PrivateRoute>
  <PrivateRoute exact path={routes.detailproduct} component={DetailProduct}></PrivateRoute>
  {/* <PrivateRoute exact path={routes.create} component={Create}></PrivateRoute>
  <PrivateRoute exact path={routes.edit} component={Edit}></PrivateRoute> */}
  {/* <Route exact path={routes.home} component={Home}></Route> */}
  </Switch>




    </div>
  );
}

export default App;
